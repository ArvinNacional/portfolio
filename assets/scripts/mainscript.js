const header = document.querySelector('.main-header');

window.addEventListener('scroll', () => {
    const scrollPos = window.scrollY;
    if(scrollPos > 10) {
        header.classList.add('scrolled');
    } else {
        header.classList.remove ('scrolled');
    }
});


const hero1 = document.querySelector('.hero1');
const slider = document.querySelector('.slider');
const headline = document.querySelector('.headline');


const tl = new TimelineMax();

tl.fromTo(hero1,1, {height: "0%"}, {height: "80%", ease: Power2.easeInOut})
.fromTo(hero1,1.2, {width: '100%'}, {width: '90%'})
.fromTo(slider,1, {x: '-100%'}, {x: '0%' ,ease:Power2.easeInOut})
.fromTo(headline,1.2, {x: '-200%'}, {x: '0%' ,ease:Power2.easeInOut})
